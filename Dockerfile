FROM python:3.7

ENV PYTHONUNBUFFERED=1 COLUMNS=200 TZ=Asia/Almaty
# Adds our application code to the image
WORKDIR /app
# Adds our application src to the image
RUN pip3 install --upgrade pip
# Install project dependencies
COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt
COPY project/ .
EXPOSE 8000
RUN ls -a