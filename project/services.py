from sqlalchemy import orm

import models
from sql_app import database
from schemas import ImageCreate


def create_database():
    return database.Base.metadata.create_all(bind=database.engine)


def get_db():
    db = database.SessionLocal()

    try:
        yield db
    finally:
        db.close()


def create_image(db: orm.Session, image: ImageCreate):
    db_image = models.Image(
        image=image.image,
        image_x2_compressed=image.image_x2_compressed,
        image_x3_compressed=image.image_x3_compressed
    )
    db.add(db_image)
    db.commit()
    db.refresh(db_image)
    return db_image
