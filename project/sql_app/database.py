import sqlalchemy as sql
from sqlalchemy.ext import declarative
from sqlalchemy import orm

SQLALCHEMY_DATABASE_URL = "sqlite:///./sql_app.db"

engine = sql.create_engine(
    SQLALCHEMY_DATABASE_URL, connect_args={"check_same_thread": False}
)

SessionLocal = orm.sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base = declarative.declarative_base()
