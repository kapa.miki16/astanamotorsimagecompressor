import sqlalchemy as sql
from sqlalchemy_utils import URLType

import database


class Image(database.Base):
    __tablename__ = 'images'

    id = sql.Column(
        sql.Integer,
        primary_key=True,
        index=True
    )
    image = sql.Column(
        URLType
    )
    image_x2_compressed = sql.Column(
        URLType
    )
    image_x3_compressed = sql.Column(
        URLType
    )
