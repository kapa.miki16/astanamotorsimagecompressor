import pydantic


class ImageBase(pydantic.BaseModel):
    image: str
    image_x2_compressed: str
    image_x3_compressed: str

    class Config:
        orm_mode = True


class Image(ImageBase):
    id: int


class ImageCreate(ImageBase):
    pass

