import os
import shutil

import fastapi
from fastapi import FastAPI, UploadFile, File
from fastapi.responses import FileResponse, JSONResponse
from sqlalchemy import orm

import services
from schemas import ImageCreate
from utils import (
    compress_image,
    UPLOAD_IMAGE_RESPONSES,
    FILE_NOT_AN_IMAGE_MESSAGE,
    FILE_NOT_FOUND_MESSAGE,
    GET_MEDIA_RESPONSES
)

app = FastAPI()

services.create_database()

media_path = os.getcwd() + '/media'


@app.get('/media/{full_filename}', responses=GET_MEDIA_RESPONSES)
def get_media(full_filename: str):
    filename, file_extension = os.path.splitext(full_filename)

    file_path = os.path.join(media_path, full_filename)
    if os.path.exists(file_path):
        return FileResponse(file_path, media_type=f'image/{file_extension}', filename=full_filename)
    return JSONResponse(status_code=404, content={"message": FILE_NOT_FOUND_MESSAGE})


@app.post("/upload_image/", responses=UPLOAD_IMAGE_RESPONSES)
def upload_image(file: UploadFile = File(...), db: orm.Session = fastapi.Depends(services.get_db)):
    filename = file.filename

    if filename.lower().endswith(('.png', '.jpg', '.jpeg')):
        file_location = f'media/{filename}'

        with open(file_location, "wb+") as file_object:
            shutil.copyfileobj(file.file, file_object)

        image_create_data = ImageCreate(
            image=file_location,
            image_x2_compressed=compress_image(file, 2),
            image_x3_compressed=compress_image(file, 3),
        )
        return services.create_image(db=db, image=image_create_data)
    else:
        return JSONResponse(status_code=400, content={"message": FILE_NOT_AN_IMAGE_MESSAGE})
