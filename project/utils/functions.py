from PIL import Image


def compress_image(image, x):
    picture = Image.open(image.file)
    filename = f'compressed_x{x}_{image.filename}'
    file_location = f"media/{filename}"
    picture.save(file_location, optimize=True, qulity=1/x*100)
    return file_location
