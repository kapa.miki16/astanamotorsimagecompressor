from .const import FILE_NOT_AN_IMAGE_MESSAGE, FILE_NOT_FOUND_MESSAGE


UPLOAD_IMAGE_RESPONSES = {
    400: {
        "description": "Fail",
        "content": {
            "application/json": {
                "example": {"message": FILE_NOT_AN_IMAGE_MESSAGE}
            }
        }
    },
    200: {
        "description": "Success",
        "content": {
            "application/json": {
                "example": {
                    'image': 'filename',
                    'image_x2_compressed': 'compressed_x2_filename',
                    'image_x3_compressed': 'compressed_x3_filename'
                }
            }
        },
    },
}

GET_MEDIA_RESPONSES = {
    404: {
        "description": "Fail",
        "content": {
            "application/json": {
                "example": {"message": FILE_NOT_FOUND_MESSAGE}
            }
        }
    },
}
